package entidades;

public class Modelo {
    String nombre;
    int catPersonas;

    public Modelo(String nombre, int catPersonas) {
        this.nombre = nombre;
        this.catPersonas = catPersonas;
    }

    @Override
    public String toString() {
        return "{" + "nombre=" + nombre + ", catPersonas=" + catPersonas + '}';
    }
    
    public void alta(){
    
    }
    
    public void baja(){
    
    }
    
    public void modificar(){
    
    }
}
