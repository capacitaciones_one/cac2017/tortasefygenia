package entidades;

public class Tortas {

    Modelo modelo;
    double precio;
    Relleno relleno;
    String decoracion;
    
    public Tortas(Modelo modelo, Relleno relleno, String decoracion, double precio) {
        this.modelo = modelo;
        this.precio = precio;
        this.relleno = relleno;
        this.decoracion = decoracion;
    }

    @Override
    public String toString() {
        return "Tortas{" + "modelo=" + modelo + ", precio=" + precio + ", relleno=" + relleno + ", decoracion=" + decoracion + '}';
    }

    public Modelo getModelo() {
        return modelo;
    }

    public void setModelo(Modelo modelo) {
        this.modelo = modelo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Relleno getRelleno() {
        return relleno;
    }

    public void setRelleno(Relleno relleno) {
        this.relleno = relleno;
    }

    public String getDecoracion() {
        return decoracion;
    }

    public void setDecoracion(String decoracion) {
        this.decoracion = decoracion;
    }

    

    public String comprar() {
        return null;
    }

}
