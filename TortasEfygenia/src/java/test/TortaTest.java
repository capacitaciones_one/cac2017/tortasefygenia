package test;

import entidades.Modelo;
import entidades.Relleno;
import entidades.Tortas;

public class TortaTest {
    public static void main(String[] args) {
        System.out.println("[...]");
        //CREAR LOS RELLENOS
        Relleno miRelleno1 = new Relleno("Fruta");
        Relleno miRelleno2 = new Relleno("Galleta");
        Relleno miRelleno3 = new Relleno("Crema");
        
        System.out.println(miRelleno1);
        System.out.println(miRelleno2);
        System.out.println(miRelleno3);
        
        //CREAR MODELOS
        Modelo miModeo1 = new Modelo("Cuatro pisos", 100);
        Modelo miModeo2 = new Modelo("2 pisos", 40);
        Modelo miModeo3 = new Modelo("Convencional", 20);
        
        System.out.println(miModeo1);
        System.out.println(miModeo2);
        System.out.println(miModeo3);
        
        
        //CREAR TORTA
        Tortas miTorta = new Tortas(miModeo1, miRelleno3, "Barroco", 3000);
        
        System.out.println(miTorta);
        System.out.println("[OK]");
    }
    
}
